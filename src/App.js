import React, { Component } from 'react';
import './styles/index.less';
import Header from './compoments/Header';
import Main from './compoments/Main';

class App extends Component {
  render() {
    return (
      <div className="container">
        <Header></Header>
        <Main></Main>
      </div>
    );
  }
}

export default App;
