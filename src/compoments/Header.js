import React, { Component } from 'react';
import HeaderImg from '../assets/avatar.jpg';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    //console.log('Todolist.constructor');
  }

  render() {
    return (
      <header>
        <div className="centerHelper">
          <img src={HeaderImg} />
        </div>
        <h1>HELLO,</h1>
        <h2 id="nameAndAge">MY NAME IS KAMIL 24YO AND THIS IS MY RESUME/CV</h2>
      </header>
    );
  }
}

export default Header;
